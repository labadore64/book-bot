module.exports = {
	name: 'booksearchdo',
	ignore:true,
	execute(message,args) {
		if(!args){
			args =[];
		}

        const { book_url } = require('../config.json');

		//gets hoot for messages.
		const common = require('../script/common.js');
		const max_page = 10;
		
		const { nickname,img_avatar,embed_color } = require('../preference.json');
		
		var EventEmitter = require("events").EventEmitter;
		var search = new EventEmitter();
		var querySearch = "";
		// Extract the required classes from the discord.js module
		const { Attachment } = require('discord.js');

		// Import the native fs module
		const fs = require('fs');
		var pagebook = require('../obj/page.js');

        const glob = require('glob');

        var count = 0;

        if(args.length == 0){
            common.Hoot(message,"You must specify a search term!");
            return;
        }

        args[0] = args[0].replace(" ","-");

        var generatedPattern = "";

        var chars = args[0].split('');

        for(var i = 0; i < chars.length; i++){
            generatedPattern += "[" + chars[i].toUpperCase() + chars[i].toLowerCase() + "]"
        }

		SearchFile(args);
		
		//listeners
		
		search.on('update', function () {
			if(!search.data || search.data.total_count == 0){
				common.Hoot(message,"I couldn't find anything for ``"+querySearch+"``, sorry...");
				return;
			}
			if(search.data.total_count == 1){
				common.SendFile(search.data[0],message)
			} else {
				pagebook.AllResults = search.data;
				pagebook.UpdatePage(0,max_page);
				search.data = {};
				AskFile();
			}
		});
		
		//end listeners
		
		
		function SearchFile(args){
			var searchString = 'test';
			if(args.length > 0){
				searchString = args[0];
			}

            glob(book_url + '/**/*'+generatedPattern+'*.{pdf,epub}', {}, (err, files)=>{
				search.data = files;
				querySearch=searchString;
				search.emit('update');
            })
		}
		
		function AskFile(){
			var string_message = "Select which file by ``"+querySearch.replace("-"," ") +"`` you want to retrieve.\n\n";
			string_message = pagebook.ListString(string_message,max_page);
			var args = [];
			args[1] = message;
			pagebook.PageMessage(message,string_message,12000,max_page,AskFile,common.SendFile,args);
		}
	}
};
