module.exports = {
	name: 'hoot',
	description: 'Repeats the text.',
	aliases: ['repeat','echo'],
	execute(message,text) {

		const common = require('../script/common.js');
		common.Hoot(message,text);
	}
};
