module.exports = {
	name: 'source',
	description: 'Links the source code.',
	execute(message,text) {
		//gets hoot for messages.
		const common = require('../script/common.js');
		
		var liblink = "https://gitlab.com/labadore64/book-bot";
		
		common.Hoot(message,"Source code:\n\n"+liblink);
	}
};
