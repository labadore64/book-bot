module.exports = {
	CleanString(str){
        const path = require("path");

		str = str.replace(/_/g, ' ');
		str = str.replace(/-/g, ' ');
        str = path.basename(str);
		return str;
	},
	SendFile(fileID,message){

		const hoot = message.client.commands.get('hoot');
		const { Attachment } = require('discord.js');

        const fs = require("fs");
        const path = require("path");

        stats = fs.statSync(fileID);
        fileSizeInBytes = stats.size;
    
        if((fileSizeInBytes / 1000000.0) > 8){
			hoot.execute(message,"That file is too big to send!");
        }

		if(!fs.existsSync(fileID)){
			hoot.execute(message,"I'm having trouble finding that file right now...");
			return;
		}

		// Send the attachment in the message channel with a content
		hoot.execute(message,"Fetching ``"+path.basename(fileID)+"``...");
		// Create the attachment using MessageAttachment
        const attachment = new Attachment(fileID);
        // Send the attachment in the message channel with a content
        message.channel.send(`${message.author},`, attachment);
	},
	Hoot(message,text){
		const { Client, RichEmbed } = require('discord.js');
		const { nickname,img_avatar,embed_color } = require('../preference.json');
		
		const embed = new RichEmbed()
		  .setTitle(nickname)
		  .setColor(embed_color)
		  .setThumbnail(img_avatar) 
		  .setDescription(text);
		message.channel.send(embed);
	}
	
};
